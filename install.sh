# Install git
sudo apt-get install git
git clone https://gitlab.com/scootles.olson/solar-hound-release.git solar_hound

cd solar_hound

sudo ln -sf /home/pi/solar_hound/system/solar-hound.service /etc/systemd/system/solar-hound.service
# Not needed? systemctl enable solar-hound.service

# To enable both PWM pins, add `dtoverlay=pwm-2chan` to `/boot/config.txt`

# Add to `/etc/udev/rules.d/99-com.rules` to allow non-sudo access to PWM
# SUBSYSTEM=="pwm*", PROGRAM="/bin/sh -c '\
#     chown -R root:gpio /sys/class/pwm && chmod -R 770 /sys/class/pwm;\
#     chown -R root:gpio /sys/devices/platform/soc/*.pwm/pwm/pwmchip* &&\
#     chmod -R 770 /sys/devices/platform/soc/*.pwm/pwm/pwmchip*\
# '"

# Enable wifi
# wpa_passphrase <password_here>
# sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
# Add:
# network={
#   ssid="<your_ssid>"
#   psk="<your_hashed_psk_from_above>"
# }

# Reconfigure wifi interface with `wpa_cli -i wlan0 reconfigure`

# Install ZeroTier
# curl -s 'https://pgp.mit.edu/pks/lookup?op=get&search=0x1657198823E52A61' | gpg --import && if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi

# Join SolarHound network
# sudo zerotier-cli join d3ecf5726d867238

# Make sure logs get rotated
# Add the following to /etc/logrotate.d/solar-hound
# /home/pi/solar-hound/logs/*.log {
#   daily
#   missingok
#   rotate 30
#   compress
#   delaycompress
# }
